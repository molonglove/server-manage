package meblog.online.server.controller;

import meblog.online.server.service.ChannelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("test")
public class TestController {

    @Autowired
    private ChannelService channelService;

    @GetMapping("ws")
    public void send() {
        for (int i = 0; i < 10; i++) {
            channelService.sendMessage(1, "this is " + i + " message !");
        }
    }

}
