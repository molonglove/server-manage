package meblog.online.server.controller;

import meblog.online.server.entity.Server;
import meblog.online.server.message.ServerInsertVo;
import meblog.online.server.message.ServerSimpleVo;
import meblog.online.server.message.ServerUpdateVo;
import meblog.online.server.service.ServerService;
import meblog.online.server.utils.PageMessage;
import meblog.online.server.utils.ResultCode;
import meblog.online.server.utils.ResultMessage;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("server")
public class ServerController {

    private final ServerService serverService;

    public ServerController(ServerService serverService) {
        this.serverService = serverService;
    }

    /**
     * 获取服务器列表
     * @param page
     * @param limit
     * @return
     */
    @GetMapping("list")
    public ResultMessage<PageMessage<ServerSimpleVo>> list(@RequestParam(value = "page", defaultValue = "1") Long page,
                                                           @RequestParam(value = "limit", defaultValue = "10") Long limit) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.list(page, limit));
    }

    /**
     * 创建连接
     * @param insertVo
     * @return
     */
    @PostMapping("add")
    public ResultMessage<ServerSimpleVo> insert(@RequestBody @Validated ServerInsertVo insertVo) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.insertServer(insertVo));
    }

    /**
     * ping测试是否服务是否连通
     * @param id
     * @return
     */
    @GetMapping("test")
    public ResultMessage<String> ping(@RequestParam("id") Integer id) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.ping(id));
    }

    /**
     * 复制服务
     * @param id
     * @return
     */
    @GetMapping("copy")
    public ResultMessage<ServerSimpleVo> copy(@RequestParam("id") Integer id) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.copy(id));
    }

    /**
     * 删除服务
     * @param ids
     * @return
     */
    @GetMapping("delete")
    public ResultMessage<Boolean> delete(@RequestParam("ids") String ids) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.delete(ids));
    }

    /**
     * 获取某一个服务
     * @param id
     * @return
     */
    @GetMapping("one")
    public ResultMessage<Server> one(@RequestParam("id") Integer id) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.one(id));
    }

    /**
     * 更新某一个服务
     * @param param
     * @return
     */
    @PostMapping("update")
    public ResultMessage<ServerSimpleVo> update(@RequestBody @Validated ServerUpdateVo param) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.update(param));
    }

    @GetMapping("cmd")
    public ResultMessage<Boolean> command(@RequestParam("cmd") String cmd) {
        return new ResultMessage<>(ResultCode.SUCCESS, serverService.cmd(cmd));
    }



}
