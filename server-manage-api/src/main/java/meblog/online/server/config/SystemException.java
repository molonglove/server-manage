package meblog.online.server.config;

import lombok.Getter;
import lombok.Setter;
import meblog.online.server.utils.ResultCode;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author: Eternity.麒麟
 * @description: 系统异常
 * @date: 2022/10/17 10:54
 * @version: 1.0
 */
@Getter
@Setter
public class SystemException extends RuntimeException implements Serializable {
    @Serial
    private static final long serialVersionUID = -8320739615591313236L;

    protected Integer code;

    protected String msg;

    public SystemException() {
        super();
    }

    public SystemException(Integer code, String msg, Throwable throwable) {
        super(msg, throwable);
        this.code = code;
        this.msg = msg;
    }

    public SystemException(ResultCode resultCode) {
        this(resultCode.getCode(), resultCode.getMsg(), null);
    }



}
