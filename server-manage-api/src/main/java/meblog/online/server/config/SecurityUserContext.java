package meblog.online.server.config;

import meblog.online.server.entity.User;
import org.springframework.core.NamedThreadLocal;

public class SecurityUserContext {

    private static final ThreadLocal<User> threadLocal = new NamedThreadLocal<>("userInfo");

    public static void addUserInfo(User user) {
        threadLocal.set(user);
    }

    public static User getUserInfo() {
        return threadLocal.get();
    }

    public static void removeUser() {
        threadLocal.remove();
    }

}
