package meblog.online.server.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class PageMessage<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = -1618963489050777040L;

    private Long page;

    private Long limit;

    private Long total;

    private List<T> records;

    public PageMessage(Long page, Long limit, Long total) {
        this(page, limit, total, null);
    }

}
