package meblog.online.server.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serial;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ResultMessage<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = -4999105416608181735L;

    private Integer code;

    private String msg;

    private T data;

    public ResultMessage(ResultCode result, T data) {
        this(result.getCode(), result.getMsg(), data);
    }

}

