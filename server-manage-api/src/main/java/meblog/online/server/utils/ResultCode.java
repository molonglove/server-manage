package meblog.online.server.utils;

import lombok.Getter;

@Getter
public enum ResultCode {

    SUCCESS(200, "成功"),

    FAIL(500, "失败"),

    /**
     * 用户不存在
     */
    USER_NOT_EXIST(501, "用户不存在"),

    AUTH_NOT_EXIST(502, "认证信息不存在"),

    /**
     * 用户认证失败，账号密码失败
     */
    USER_IDENTIFICATION(503, "用户认证失败，账号密码失败"),

    /**
     * 认证信息格式不正确
     */
    JWT_PARSE_FAIL(504, "认证信息格式不正确"),

    /**
     * 认证信息过期
     */
    AUTH_TIME_EXPIRED(505, "认证信息过期"),

    /**
     * 数据不存在
     */
    DATA_NOT_EXIST(506, "数据不存在"),

    /**
     * 数据已经存在
     */
    DATA_EXIST(507, "数据已经存在"),

    /**
     * 服务标题存在
     */
    SERVER_TITLE_EXIST(508, "服务标题存在"),

    SERVER_CONNECT_FAIL(509, "远程服务连接失败"),
    ;

    private final Integer code;

    private final String msg;

    ResultCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
