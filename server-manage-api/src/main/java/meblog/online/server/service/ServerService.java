package meblog.online.server.service;

import meblog.online.server.entity.Server;
import com.baomidou.mybatisplus.extension.service.IService;
import meblog.online.server.message.ServerInsertVo;
import meblog.online.server.message.ServerSimpleVo;
import meblog.online.server.message.ServerUpdateVo;
import meblog.online.server.utils.PageMessage;

/**
* @author yuelong
* @description 针对表【m_server(服务器列表)】的数据库操作Service
* @createDate 2022-11-22 20:27:58
*/
public interface ServerService {

    /**
     * 增加服务器信息
     * @param insertVo
     * @return
     */
    ServerSimpleVo insertServer(ServerInsertVo insertVo);

    /**
     * 服务器列表
     * @param page
     * @param limit
     * @return
     */
    PageMessage<ServerSimpleVo> list(Long page, Long limit);

    /**
     * 复制服务
     * @param id
     * @return
     */
    ServerSimpleVo copy(Integer id);

    /**
     * 删除服务
     * @param ids
     * @return
     */
    Boolean delete(String ids);

    /**
     * 获取某一个服务
     * @param id
     * @return
     */
    Server one(Integer id);

    /**
     * 更新某一个服务
     * @param param
     * @return
     */
    ServerSimpleVo update(ServerUpdateVo param);

    /**
     * ping服务器
     * @param id
     * @return
     */
    String ping(Integer id);

    /**
     * 执行cmd命令
     * @param cmd
     * @return
     */
    Boolean cmd(String cmd);
}
