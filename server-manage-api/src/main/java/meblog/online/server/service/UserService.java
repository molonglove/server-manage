package meblog.online.server.service;

import meblog.online.server.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import meblog.online.server.message.UserLoginVo;
import meblog.online.server.message.UserSimpleVo;

/**
* @author yuelong
* @description 针对表【m_user(系统用户)】的数据库操作Service
* @createDate 2022-11-22 17:05:50
*/
public interface UserService {

    /**
     * 登陆
     * @param userLoginVo
     * @return
     */
    UserSimpleVo login(UserLoginVo userLoginVo);
}
