package meblog.online.server.service.impl;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import meblog.online.server.service.DockerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DockerServiceImpl implements DockerService {

    @Resource
    private DockerClient dockerClient;

    public List<Container> containers() {
        List<Container> list = dockerClient.listContainersCmd().withShowAll(true).exec();
        return list;
    }



}
