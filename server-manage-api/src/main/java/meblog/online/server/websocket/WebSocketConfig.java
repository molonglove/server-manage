package meblog.online.server.websocket;

public class WebSocketConfig {

    /**
     * 默认端口
     */
    public static final Integer PORT = 8081;

    /**
     * BOSS线程名
     */
    public static final String BOSS_THREAD_NAME = "ws-http-boss";

    /**
     * worker线程名
     */
    public static final String WORKER_THREAD_NAME = "ws-http-worker";

    /**
     * 业务执行线程名
     */
    public static final String EXECUTOR_THREAD_NAME = "ws-http-executor";

    /**
     * 套接口排队的最大连接个数
     */
    public static final Integer BACKLOG = 8192;

}
