package meblog.online.server.message;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import meblog.online.server.entity.Server;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ServerSimpleVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 3852028754569228554L;

    private Integer id;

    /**
     * 服务器名称
     */
    private String title;

    /**
     * 地址
     */
    private String host;

    /**
     * 端口
     */
    private Integer part;

    /**
     * 备注
     */
    private String brief;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    public static ServerSimpleVo build(Server server) {
        return new ServerSimpleVo(server.getId(), server.getTitle(), server.getHost(), server.getPort(), server.getBrief(), server.getCreateTime());
    }

}
