package meblog.online.server.message;

import lombok.Data;
import lombok.experimental.Accessors;
import meblog.online.server.entity.Server;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class ServerInsertVo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1676403556135761827L;

    /**
     * 服务器名称
     */
    @NotEmpty(message = "服务器标题不能为空")
    private String title;

    /**
     * 地址
     */
    @NotEmpty(message = "服务器地址不能为空")
    private String host;

    /**
     * 端口
     */
    @NotNull(message = "服务器端口不能为空")
    private Integer port;

    /**
     * 用户名
     */
    @NotEmpty(message = "服务器用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotEmpty(message = "服务器密码不能为空")
    private String password;

    /**
     * 备注
     */
    private String brief;

    /**
     * 构建模型
     * @return
     */
    public Server formBuild() {
        return new Server()
                .setTitle(title)
                .setHost(host)
                .setPort(port)
                .setBrief(brief)
                .setCanView(true)
                .setUpdateTime(LocalDateTime.now())
                .setCreateTime(LocalDateTime.now())
                .setUsername(username)
                .setPassword(password);
    }

}
