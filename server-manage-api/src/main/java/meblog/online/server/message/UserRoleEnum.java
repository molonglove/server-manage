package meblog.online.server.message;

import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

public enum UserRoleEnum {

    SUPER(1, "超级用户"),
    ADMIN(2, "管理员"),
    USER(3, "普通用户"),
    VISITOR(4, "游客")
    ;

    @Getter
    private Integer type;

    @Getter
    private String name;

    UserRoleEnum(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public static String name(Integer type) {
        return Arrays.stream(UserRoleEnum.values())
                .filter(item -> Objects.equals(item.type, type))
                .findFirst()
                .orElse(VISITOR).getName();
    }

}
