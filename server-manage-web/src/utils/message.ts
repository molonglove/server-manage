import {Notification} from "@douyinfe/semi-ui";

/**
 * 错误
 * @param content
 * @param duration
 * @constructor
 */
export const Error = (content: string, duration = 3) => Notification.error({content: content, title: '错误', duration: duration})

/**
 * 成功
 * @param content
 * @param duration
 * @constructor
 */
export const Success = (content: string, duration = 3) => Notification.success({content: content, title: '提示', duration: duration})