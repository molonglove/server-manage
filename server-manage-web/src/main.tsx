import React from 'react'
import ReactDOM, {createRoot} from 'react-dom/client'
import {Application} from "./pages";
import "reset-css/reset.css"

const rootElement = document.getElementById('root');
const root = createRoot(rootElement!);

root.render(
    <Application />
);


// ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
//   <React.StrictMode>
//     <Application />
//   </React.StrictMode>
// )
