import React, {useReducer, useState} from "react";
import {Button, Card, Form, Input, Modal} from "@douyinfe/semi-ui";
import styled from "@emotion/styled";
import { IconUser, IconKey } from '@douyinfe/semi-icons';
import {httpPost, Result} from "../../utils/http";
import {UserState} from "../../redux/user/reducer";
import {useDispatch} from "react-redux";
import {changeLoginStatusActionCreator} from "../../redux/user/action";
import {useNavigate} from "react-router-dom";

interface LoginProp {
    username: string;
    password: string;
}

export const LoginPage = () => {

    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [loginParam, setLoginParam] = useState<LoginProp>({username: '', password: ''});
    const login = () => {
        if (loginParam.password === '' || loginParam.username == '') {
            Modal.error({ title: '提示', content: '账号和密码不能为空' });
        } else {
            httpPost<Result<UserState>, LoginProp>('/login/index', loginParam).then(res => {
                if (res.code === 200) {
                    Modal.success({title: '提示', content: '登陆成功'})
                    dispatch(changeLoginStatusActionCreator({...res.data, status: true}))
                    navigate('/manage/script')
                } else {
                    Modal.error({ title: '错误', content: res.msg })
                }
            }).catch(err => {
                Modal.error({ title: '错误', content: err })
            })
        }
    }

    return <Container>
        <Card style={{width: 350}}>
            <Form>
                <h3>Lucky.YL</h3>
                <Input
                    value={loginParam.username}
                    onChange={e => setLoginParam({...loginParam, username: e})}
                    style={{marginTop: 12, marginBottom: 12}}
                    prefix={<IconUser />}
                    size='large'
                    placeholder='请输入账号'
                />
                <Input
                    value={loginParam.password}
                    onChange={e => setLoginParam({...loginParam, password: e})}
                    style={{marginTop: 12, marginBottom: 12}}
                    mode="password"
                    prefix={<IconKey />}
                    size='large'
                    placeholder='请输入密码'
                />
                <Button
                    style={{marginTop: 12, marginBottom: 12}}
                    theme='solid'
                    type='primary'
                    block={true}
                    size='large'
                    onClick={login}
                >登 陆</Button>
            </Form>
        </Card>
    </Container>
}

const Container = styled.div`
  background: url("https://meblog-online-storage.oss-cn-beijing.aliyuncs.com/accessory/login.jpg") no-repeat;
  background-size: 100% 100%;
  height: 100vh;
  overflow: hidden;
  width: 100vw;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-content: center;
  justify-content: center;
  align-items: center;
  h3 {
    font-size: 24px;
    font-weight: 600;
    text-align: center;
    margin: 10px auto 20px;
  }
`