import React, {useEffect, useLayoutEffect, useState} from "react";
import {FormApi} from "@douyinfe/semi-ui/lib/es/form";
import {httpGet, httpPost, Result} from "../../utils/http";
import {Error, Success} from "../../utils/message";
import {Button, Form, Modal, Spin} from "@douyinfe/semi-ui";
import {IconClose, IconTick} from "@douyinfe/semi-icons";

interface ServerUpdateProp {
    visible: boolean;
    id: number;
    closeModal: (isLoad: boolean) => void;
}

interface ServerFromProp {
    id: number;
    title: string;
    host: string;
    port: number;
    username: string;
    password: string;
    brief: string;
}

export const ServerUpdate: React.FC<ServerUpdateProp> = ({visible, id, closeModal}) => {

    const [serverFromProp, setServerFromProp] = useState<ServerFromProp | undefined>(undefined);

    const [formApi, setFormApi] = useState<FormApi<ServerFromProp>>();

    const [spinning, setSpinning] = useState<boolean>(false);

    const handleAfterClose = () => formApi?.reset();

    const handleCancel = (isLoad: boolean) => {
        formApi?.reset();
        closeModal(isLoad);
    }

    const handleOk = (values: any) => {
        console.log("values", values);
        setSpinning(true);
        httpPost<Result<boolean>, any>('/server/update', {...values, id: id}).then(res => {
            setSpinning(false);
            if (res.code === 200) {
                Success('修改成功');
                handleCancel(true);
            } else {
                Error(res.msg);
            }
        }).catch(err => {
            setSpinning(false);
            Error(err);
        })
    }


    useEffect(() => {
        if (visible) {
            setSpinning(true);
            httpGet<Result<ServerFromProp | string>, any>('/server/one', {id: id}).then(res => {
                setSpinning(false);
                if (res.code === 200) {
                    setServerFromProp(res.data as ServerFromProp);
                    formApi?.setValues(res.data as ServerFromProp);
                } else {
                    Error(res.data as string)
                }
            }).catch(err => {
                setSpinning(false);
            })
        } else {
            setServerFromProp(undefined);
            formApi?.reset();

        }
    }, [visible])

    return <Modal
        title="修改服务"
        visible={visible}
        maskClosable={false}
        onOk={handleOk}
        afterClose={handleAfterClose}
        onCancel={() => handleCancel(false)}
        closeOnEsc={true}
        footer={<>
            <Button type="danger" icon={<IconClose />} theme={'solid'} onClick={() => handleCancel(false)}>取消</Button>
            <Button type="primary" icon={<IconTick />} theme={'solid'} onClick={() => formApi?.submitForm()}>确认</Button>
        </>}
    >
        <Spin spinning={spinning} tip={'加载中......'}>
            { serverFromProp !== undefined && <Form labelPosition={"left"} initValues={serverFromProp} getFormApi={setFormApi} onSubmit={handleOk} labelCol={{span: 4}} wrapperCol={{span: 20}}>
                <Form.Input field='title' showClear label='名称' rules={[{required: true, message: '服务器名称未填写'}]}/>
                <Form.Input field='host' showClear label='地址' rules={[{required: true, message: '服务器地址未填写'}]}/>
                <Form.InputNumber field='port' label='端口' style={{width: '100%'}} defaultValue={22}/>
                <Form.Input field='username' showClear label='账号' rules={[{required: true, message: '服务器用户名未填写'}]}/>
                <Form.Input field='password' showClear mode={'password'} label='密码' rules={[{required: true, message: '服务器密码未填写'}]}/>
                <Form.TextArea maxCount={150} showClear field='brief' label='备注'  />
            </Form>}
        </Spin>
    </Modal>

}