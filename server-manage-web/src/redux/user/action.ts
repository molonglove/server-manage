import {UserState} from "./reducer";

export const CHANGE_LONGIN_STATUS = "change_login_status";

interface ChangLoginAction {
    type: typeof CHANGE_LONGIN_STATUS;
    payload: UserState;
}

export type UserTypes = ChangLoginAction;

export const changeLoginStatusActionCreator = (status: UserState): ChangLoginAction => {
    return {
        type: CHANGE_LONGIN_STATUS,
        payload: status
    }
}